# name:  procsealeveldata.R
#
# description: annual mean values of sea level from several sources, and the
# local long-term trend. Only to be called if new data are available, or expected.
#
# usage: In a unix shell, type:
#
#        Rscript procsealeveldata.R
#
# return:
#       A file called "zeespiegel_jaar.csv" containing the annual means for past years
#       of sea level rel. to NAP (local, an average of values for 6 stations), its
#       long-term trend (localtrend), the sea level shifted vertically to have mean
#       zero over a reference period (local0), its long-term trend (local0trend),
#       reconstruction of global mean sea level GIA corrected (reconstruction) and global
#       mean sea level from satellite radar altimeter data, GIA corrected (altimetry).
#       The file is stored in the folder sealeveldata; its full path name is stored in the
#       string datapath to be modified in the code below.
#
# details:
#       The global sea level reconstruction data Frederikse-Global.csv must be present in the
#       subfolder Frederikse of the folder sealeveldata (see above). It is fixed data from
#       Frederikse et al (2020). The subfolder NASA needs to exist and contain the file
#       nasa-url.txt.
#
#       Global mean sea level data from altimetry (NASA) and the local sea level data for
#       the Netherlands are retrieved automatically.
#
#       A "starting version" of the folder sealeveldata is included in the zip file
#       sealeveldata.zip.
#
# version: 17-Feb-2022
#
# author: Cees de Valk \email{cees.de.valk@knmi.nl}
#

# paths to source and sea level data (to be modified!)
datapath <- "/Users/ceesdevalk/Documents/KNMI/Projects/sealeveldata"
sourcepath <- "/Users/ceesdevalk/Documents/KNMI/Projects/trend_knmi/R"

source(file.path(sourcepath, "readfrederikse.R"))
source(file.path(sourcepath, "readnasa.R"))
source(file.path(sourcepath, "readpsmsl.R"))
source(file.path(sourcepath, "climatrend.R"))
source(file.path(sourcepath, "sealevelslope.R"))

# constants
# period over which global and local sealevel must be aligned vertically
refyears <- 1901:2000 
nlrefyears <- 1996:2014 
firstyear <- 1900     # first year to be shown on dashboard
bootsize <- 10000
p <- 0.9
lag <- 16
blocksize <- 10

# get data either from the source (psmsl) or already downloaded
# (altimetry, reconstruction)
hoofdstationsnap <- readpsmsl(datapath)
ns1 <- dim(hoofdstationsnap)[2]
temp <- apply(hoofdstationsnap[,2:ns1], 1, mean)
refmean <- mean(temp[hoofdstationsnap$year %in% nlrefyears])
temp <- temp-refmean
local <- data.frame(year= hoofdstationsnap$year, level= round(temp, 3))
reconstruction <- readfrederikse(datapath)
altimetry <- readnasa(datapath)

# align level of altimetry data with level of reconstruction
is <- intersect(reconstruction$year, altimetry$year)
irec <- reconstruction$year%in%is
ialt <- altimetry$year%in%is
altimetry$level <- altimetry$level+mean(reconstruction$level[irec])-mean(altimetry$level[ialt])

# create splice of aligned altimetry data and reconstruction (only for
# alignment to local sea level data! this is not shown)
synthetic <- rbind(reconstruction, altimetry[!ialt,])

# align level of altimetry/reconstruction with local sea level centered
if (!(all(refyears %in% synthetic$year) & all(refyears %in% local$year))) {
  error("some data in refyears missing")
}
# is <- intersect(synthetic$year, local$year)
isyn <- synthetic$year%in%refyears
inap <- local$year%in%refyears
local0 <- local
localoffset <- -mean(local$level[inap]) # must be added
local0$level <- local0$level+localoffset
syncorrection <- mean(local0$level[inap])-mean(synthetic$level[isyn]) # must be added
altimetry$level <- altimetry$level+syncorrection
reconstruction$level <- reconstruction$level+syncorrection

# local trend and its slope
trenddata <- sealevelslope(local$year, local$level, bootsize= bootsize,
                           p= p, lag= lag, blocksize= blocksize)


iloc <- local$year>= firstyear
local <- local[iloc, ]
local0 <- local0[iloc, ]
localtrend <- trenddata$trend[iloc]
islp <- trenddata$ts>= firstyear
ts <- trenddata$ts[islp]
slope <- trenddata$slope[islp]
slopelb <- trenddata$slopelb[islp]
slopeub <- trenddata$slopeub[islp]

# write everything in one single file
year <- sort(unique(c(local$year, altimetry$year, reconstruction$year)))
iloc <- year%in%local$year
ialt <- year%in%altimetry$year
irec <- year%in%reconstruction$year
islp <- year%in%ts
output <- data.frame(year= year, local= NA, localtrend= NA, local0= NA, local0trend= NA,
                     reconstruction= NA, altimetry= NA, slope= NA, slopelb= NA, slopeub= NA)
output$local[iloc] <- round(local$level, 3)
output$local0[iloc] <- round(local0$level, 3)
output$localtrend[iloc] <- round(localtrend, 3)
output$local0trend[iloc] <- round(localtrend+localoffset, 3)
output$reconstruction[irec] <- round(reconstruction$level, 3)
output$altimetry[ialt] <- round(altimetry$level, 3)
output$slope[islp] <- round(slope, 3)
output$slopelb[islp] <- round(slopelb, 3)
output$slopeub[islp] <- round(slopeub, 3)

# write annual means and trend to file
headerl <- paste("laatste jaar: ", max(year), "; datum t.o.v. gemiddelde over: ", min(refyears), "-", max(refyears))
fout <- "zeespiegel_jaar.csv"
foutname <- file.path(datapath, fout)
write(headerl, foutname)
write.table(output, foutname,  sep = ",", append = TRUE, row.names = FALSE, na= " ")


# plots only for checking:
# png(file.path(datapath, "LocalDataNAP.png"), units="in", width=6, height=6, res=300)
# par(pty= "m")
# plot(output$year, output$localtrend, type="l",
#      xlab= "year", main= "jaargemiddelde zeespiegel relatief tot NAP",
#      ylab= "hoogte (m)",
#      col= "blue", lwd=2, ylim= c(-.2, .7), xlim= c(1900, 2100))
# grid()
# lines(c(1500,3000), c(0,0), lty=2)
# lines(output$year, output$local, col=1,lwd=.5)
# dev.off()
#
# png(file.path(datapath, "LocalGlobalData.png"), units="in", width=6, height=6, res=300)
# par(pty= "m")
# plot(output$year, output$local0trend, type="l",
#      xlab= "year", main= "Verandering in jaargemiddelde zeespiegel", ylab= "hoogte (m)",
#      lwd=2, col= "blue", ylim= c(-1, 1)*.15)
# grid()
# lines(c(1500,3000), c(0,0), lty=2)
# lines(output$year, output$local0, col=1,lwd=.5)
# lines(output$year, output$reconstruction, col= "magenta3",lwd=2)
# lines(output$year, output$altimetry, col="darkorange",lwd=2)
# dev.off()


